# Duplicate Detector  

Adds a the ability to detect duplicate titles in posts, pages and custom post types.

Look to the WordPress settings for Duplicate Detector to activate the post-types you would like DD to be active on. 
You can now also isolate searches for a particular post-type, within that post-type. For example, a serach for a duplicate title with the 'widgets' custom post-type would only return results for other widgets, not posts, pages etc.

## Requirements
* PHP >= 5.6
* WP >= 4.7